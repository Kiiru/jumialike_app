package com.example.jumialike_app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class PaydayLoan extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payday_loan, container, false);
    }

    public void onClick(View view) {

        Fragment fragment=null;
        fragment =new PaydayloanFragment();
        android.support.v4.app.FragmentManager
                fragmentManager= getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container,fragment).commit();
    }
}