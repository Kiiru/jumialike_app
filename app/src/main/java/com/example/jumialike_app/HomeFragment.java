package com.example.jumialike_app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class HomeFragment extends Fragment implements View.OnClickListener {

    private ImageView img;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_home, container, false);
        img = (ImageView) myFragmentView.findViewById(R.id.voucheri_mage);
        img.setOnClickListener(this);

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onClick(View v) {
        Fragment voucher_fragment = new VoucherBlankFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, voucher_fragment );
        transaction.addToBackStack(null);
        transaction.commit();
//        switch (v.getId()){
//            case R.id.voucheri_mage:
//                Intent i = new Intent(getContext(), Main2Activity.class);
//                startActivity(i);
//                break;
//                default:
//                    break;
//        }

    }
}