package com.example.jumialike_app;



public class AndyConstants {
    // web service url constants
    public class ServiceType {
        //public static final String BASE_URL = "https://demonuts.com/Demonuts/JsonTest/Tennis/";

        public static final String URL = "http://62.24.104.73:8888/android_user_api/api.php";

        public static final String BASE_URL ="http://62.24.104.73:8888/android_user_api/";
        public static final String LOGIN = BASE_URL + "simplelogin.php";
        public static final String REGISTER =  BASE_URL + "simpleregister.php";
        public static final String REGISTER_USER =  BASE_URL + "registerprofile.php";
    }
    // webservice key constants
    public class Params {

        public static final String NAME = "NAME";
        public static final String HOBBY = "hobby";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
    }
    public class Params2 {

        public static final String IDNO = "idno";
        public static final String LOCATION = "location";
        public static final String RESIDENCE = "residence";
        public static final String OCCUPATION = "occupation";
        public static final String OFFICE = "office";
        public static final String INCOME = "income";
    }
    public class Params_Customer {

        public static final String IDNO = "IDNO";
        public static final String LOCATION = "LOCATION";
        public static final String RESIDENCE = "RESIDENCE";
        public static final String OCCUPATION = "OCCUPATION";
        public static final String OFFICE = "OFFICE";
        public static final String INCOME = "INCOME";
    }
}