package com.example.jumialike_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    private Spinner smarket;

    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        smarket = (Spinner) findViewById(R.id.spinner1);
        submit = (Button) findViewById(R.id.submit);
        List<String> list = new ArrayList<String>();
        list.add("Tuskeys");
        list.add("Naivas");
        list.add("Uchumi");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        smarket.setAdapter(dataAdapter);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You have successfully generated the Voucher and it will be send to the number you ptovided", Toast.LENGTH_LONG).show();
            }
        });
    }
}
